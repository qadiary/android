package com.qnadiary.android.apis;

import com.qnadiary.android.models.Diary;

import java.util.List;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by Student on 2016-09-20.
 */
public interface DiaryAPI {
    final String PREFIX = "/diary";

    @GET(PREFIX + "/")    // 일기 목록
    public void list_Diary(@Query("user_id") Integer userId,
                           Callback<List<Diary>> callback);

    @POST(PREFIX + "/")   // 일기 추가
    public void add_Diary(@Query("user_id") Integer userId,
                          @Query("title") String title,
                          @Query("contents") String contents,
                          Callback<Diary> callback);

    @DELETE(PREFIX + "/")   // 일기 삭제
    public void delete_Diary(@Query("user_id") Integer userId,
                             @Query("diary_id") Integer diaryId,
                             Callback<Diary> callback);

    @PUT(PREFIX + "/")   // 일기 수정
    public void update_Diary(@Query("user_id") Integer userId,
                             @Query("diary_id") Integer diaryId,
                             @Query("title") String title,
                             @Query("contents") String contents,
                             Callback<Diary> callback);
}
