package com.qnadiary.android.apis;

import com.qnadiary.android.models.QnAData;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Student on 2016-09-06.
 */
public interface QnaAPI {
    final String PREFIX = "/qna";

    @GET(PREFIX + "/question")  // 질문 받기
    public void get_Question(@Query("user_id") Integer userId,
                             Callback<QnAData> callback);

    @GET(PREFIX + "/answer")    // 답변 목록
    public void list_Answer(@Query("user_id") Integer userId,
                            @Query("question_id") Integer questionId,
                            Callback<List<QnAData>> callback);

    @POST(PREFIX + "/")   // 답변 추가
    public void add_Answer(@Query("user_id") Integer userId,
                           @Query("question_id") Integer questionId,
                           @Query("text") String answer,
                           Callback<QnAData> callback);
}
