package com.qnadiary.android.apis;

import com.qnadiary.android.models.User;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Student on 2016-09-16.
 */
public interface UserAPI {
    final String PREFIX = "/users";

    @FormUrlEncoded
    @POST(PREFIX + "/login") // 로그인
    public void login(@Field("nickname") String nickname,
                      @Field("password") String password,
                      Callback<User> callback);

    @FormUrlEncoded
    @POST(PREFIX + "/")
    public void signUp(@Field("nickname") String nickname,
                           @Field("password") String password,
                           @Field("uuid") String device_id,
                           Callback<User> callback);
}
