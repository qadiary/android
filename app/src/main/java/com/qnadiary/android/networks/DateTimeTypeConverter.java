package com.qnadiary.android.networks;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * description
 * DateTime 형으로 들어오는 데이터를 자동으로 컨버팅해주는 클래스
 * @author LeeDaYeon
 */
public class DateTimeTypeConverter implements JsonSerializer<Date>, JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            String s = json.getAsJsonPrimitive().getAsString();
            long l = Long.parseLong(s);
            return new Date(l * 1000);
        } catch (IllegalArgumentException e) {
            return context.deserialize(json, Date.class);
        }
    }

    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
    }
}
