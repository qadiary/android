package com.qnadiary.android.networks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qnadiary.android.Config;
import com.squareup.okhttp.OkHttpClient;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Student on 2016-09-06.
 */
public class NetworkManager {
    public static NetworkManager instance = null;
    private HashMap<Class, Object> apiMap = new HashMap<>();
    RestAdapter restAdapter = null;

    NetworkManager() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat(DateFormat.FULL, DateFormat.FULL) // 날짜, 시간 포맷 지정
                .registerTypeAdapter(Date.class, new DateTimeTypeConverter())
                .create();

        OkHttpClient httpClient = new OkHttpClient();
        httpClient.setConnectTimeout(10 * 1000, TimeUnit.MILLISECONDS); // 서버연결시간이 10초 이상 초과시
        httpClient.setReadTimeout(15 * 1000, TimeUnit.MILLISECONDS); // 데이터 불러오는 시간이 15초 이상 초과시
        OkClient client = new OkClient(httpClient);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.SERVER_URI) // 서버 주소..
                .setClient(client)
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() { // 보낼 때 잠깐 멈춰서 세션같은거? 추가하는..?
                    @Override
                    public void intercept(RequestFacade request) {

                    }
                })
                .build();
    }

    public static NetworkManager getInstance() { // 싱글톤 객체 생성
        if (instance == null) { // 객체가 없을 경우에만 다시 생성
            instance = new NetworkManager();
        }
        return instance;
    }

    public <T> T getAPI(Class<T> cls) { // class(interface)에서 파마미터 같은 값들을 가져옴
        Object hit = apiMap.get(cls);
        if (hit == null) {
            hit = restAdapter.create(cls);
            apiMap.put(cls, hit);
        }
        return (T) hit;
    }
}
