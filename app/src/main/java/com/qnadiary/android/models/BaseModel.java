package com.qnadiary.android.models;

import java.io.Serializable;

/**
 * description
 * 모든 모델의 기본이 되는 모델 클래스
 * @author LeeDaYeon
 */
public class BaseModel extends Object implements Serializable {
    private Integer code;
    private String msg;
    private String data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
