package com.qnadiary.android.models;

import java.util.Date;

/**
 * Created by Student on 2016-09-04.
 */
public class QnAData extends BaseModel {
    Integer questionId = 0;
    String question = "";
    String answer = "";
    Date createdAt;

    public QnAData() {
    }

    public QnAData(Integer questionId, String question, String answer, Date createdAt) {
        this.questionId = questionId;
        this.question = question;
        this.answer = answer;
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getQ_id() {
        return questionId;
    }

    public void setQ_id(Integer question_id) {
        this.questionId = question_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
