package com.qnadiary.android.models;

/**
 * description
 *
 * @author JintaePang
 */
public class FiveTalk extends BaseModel {
    private Integer year = 0;
    private Integer month = 0;
    private Integer day = 0;
    private String five = "";

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getFive() {
        return five;
    }

    public void setFive(String five) {
        this.five = five;
    }
}
