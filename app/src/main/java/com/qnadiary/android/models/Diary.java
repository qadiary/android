package com.qnadiary.android.models;

/**
 * description
 * Calendar Diary Model을 일종의 일정이기 때문에 Diary 이라고 명명함
 * @author LeeDaYeon
 */
public class Diary extends BaseModel {
    private Integer year = 0;
    private Integer month = 0;
    private Integer day = 0;
    private String title = "";
    private String contents = "";
    private Integer diaryId = 0;

    public Diary() { }
    public Diary(Integer year, Integer month, Integer day, String title) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.title = title;
    }

    public Integer getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
