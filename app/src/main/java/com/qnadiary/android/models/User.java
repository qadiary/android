package com.qnadiary.android.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Student on 2016-09-20.
 */
public class User extends BaseModel {
    private Integer id = 0;
    private String nickname = "null";
    private String password = "null";
    private Boolean unregistered = false;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("last_login_at")
    private Date lastLoginAt;

    @SerializedName("uuid")
    private String deviceId = "null";

    public Boolean isUnregistered() {
        return this.unregistered;
    }

    public void setUnregistered(Boolean unregistered) {
        this.unregistered = unregistered;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(Date lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String device_id) {
        this.deviceId = device_id;
    }

}
