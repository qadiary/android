package com.qnadiary.android.enums;

/**
 * description
 * Intent requestCode 나 유니크한 인트 value에 사용되는 값들에 대한 정의가 되어있는 부분
 * @author LeeDaYeon
 */
public enum Actions {
    ADD_DIARY(1), // 다이어리 추가
    AMEND_DIARY(2), // 다이어리 수정

    CANCEL(9996),
    FAIL(9997),
    SUCCESS(9998),
    UNKNOWN(9999);

    private final int code;

    Actions(int code) {
        this.code = code;
    }

    public boolean equals(Actions action) {
        return this.getCode() == action.getCode();
    }

    public static Actions fromValue(int value) {
        for (Actions action : values()) {
            if (action.code == value) return action;
        }
        return UNKNOWN;
    }

    public int getCode() {
        return code;
    }
}
