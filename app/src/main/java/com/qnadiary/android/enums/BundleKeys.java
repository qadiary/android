package com.qnadiary.android.enums;

/**
 * description
 * Intent를 이용한 Bundle 값들 Key를 모아둔 부분
 * @author LeeDaYeon
 */
public enum BundleKeys {
    DIARY_ACTION_TYPE,
    YEAR,
    MONTH,
    DAY,
    SECTION_NUMBER,
    UNKNOWN;

    public String getKey() {
        return this.name();
    }
}
