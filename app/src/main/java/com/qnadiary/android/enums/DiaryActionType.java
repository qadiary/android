package com.qnadiary.android.enums;

/**
 * description
 * 일정에 대한 유저들의 액션을 분류하는 타입
 * @author LeeDaYeon
 */
public enum DiaryActionType {
    ADD(1), // 생성
    AMEND(2), // 수정
    VIEW(3); // 보기

    private final int code;

    DiaryActionType(int code) {
        this.code = code;
    }

    public boolean equals(DiaryActionType action) {
        return this.getCode() == action.getCode();
    }

    public static DiaryActionType fromValue(int value) {
        for (DiaryActionType action : values()) {
            if (action.code == value) return action;
        }
        return ADD;
    }

    public int getCode() {
        return code;
    }
}
