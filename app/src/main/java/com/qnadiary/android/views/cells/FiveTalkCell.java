package com.qnadiary.android.views.cells;

import android.content.Context;
import android.view.View;

import com.qnadiary.android.R;
import com.qnadiary.android.models.FiveTalk;
import com.qnadiary.android.views.cells.base.BaseCell;
import com.qnadiary.android.views.widgets.TextView;

/**
 * Created by Student on 2016-09-28.
 */
public class FiveTalkCell extends BaseCell<FiveTalk> {
    TextView mDateTextView;
    TextView mFiveTalkTextView;

    public FiveTalkCell(Context context) {
        super(context);
    }

    public FiveTalkCell(Context context, int position) {
        super(context, position);
    }

    public void init(View v) {
        mDateTextView = (TextView) v.findViewById(R.id.date_text_view);
        mFiveTalkTextView = (TextView) v.findViewById(R.id.five_talk_text_view);
    }

    public void bind(FiveTalk item) {
        mDateTextView.setText(item.getYear() + "년 " + item.getMonth() + "월 " + item.getDay() + "일");
        mFiveTalkTextView.setText(item.getFive());
        this.item = item;
    }
}
