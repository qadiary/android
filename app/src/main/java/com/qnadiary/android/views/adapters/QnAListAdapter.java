package com.qnadiary.android.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qnadiary.android.R;
import com.qnadiary.android.models.QnAData;
import com.qnadiary.android.views.adapters.base.ListBaseAdapter;
import com.qnadiary.android.views.cells.QnACell;

import java.util.List;

/**
 * Created by Student on 2016-09-04.
 */
public class QnAListAdapter extends ListBaseAdapter<QnAData> {

    public QnAListAdapter(Context c) {
        super(c);
    }

    public QnAListAdapter(Context c, List<QnAData> items) {
        super(c, items);
    }

    @Override
    protected void bindCell(int position, View view) {
        QnAData item = getItem(position);
        QnACell cell = (QnACell) view.getTag();
        cell.bind(item);
    }

    @Override
    protected View createCell(int position, ViewGroup viewGroup) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_qna, viewGroup, false);
        QnACell cell = new QnACell(getContext(), position);
        cell.init(view);
        view.setTag(cell);
        return view;
    }
}
