package com.qnadiary.android.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.qnadiary.android.Config;
import com.qnadiary.android.R;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.common.FiveTalkManager;
import com.qnadiary.android.models.FiveTalk;
import com.qnadiary.android.views.adapters.FiveTalkListAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FiveTalkListActivity extends BaseActivity {
    private ListView mFiveTalkListView;
    private FiveTalkListAdapter mFiveTalkListAdapter;
    private List<FiveTalk> fiveItems = new ArrayList<>();
    FiveTalkManager fiveTalkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_five_talk_list);
        fiveTalkManager = FiveTalkManager.getInstance(this);
        init();
    }

    private void init() {
        mFiveTalkListView = (ListView) findViewById(R.id.five_talk_list_view);
        onPullData();
        mFiveTalkListAdapter = new FiveTalkListAdapter(this, fiveItems);
        mFiveTalkListView.setAdapter(mFiveTalkListAdapter);
    }

    public void onPullData() {
        fiveTalkManager.onLoadData(Config.FIVE_TALK_DATA_URI);
        Calendar calendar = Calendar.getInstance();

        Integer tmpDay=0;
        Integer year = calendar.get(Calendar.YEAR);
        Integer month = calendar.get(Calendar.MONTH) + 1;
        int tmpMaxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        for(int i=0;i<tmpMaxDay;i++) {
            tmpDay++;
            String tmpKey = fiveTalkManager.makeKey(year, month, tmpDay);
            FiveTalk fivetalk = fiveTalkManager.getDiaryMap().get(tmpKey);
            if(fivetalk != null) {
                fiveItems.add(fivetalk);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        int groupId = 0;

        MenuItem cancelItem = menu.add(groupId, CANCEL_CODE, 0, R.string.cancel);
        cancelItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        cancelItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(CANCEL_CODE);
        finish();

        return super.onOptionsItemSelected(item);
    }
}
