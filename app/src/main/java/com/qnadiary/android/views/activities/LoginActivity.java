package com.qnadiary.android.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.qnadiary.android.R;
import com.qnadiary.android.apis.UserAPI;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.models.User;
import com.qnadiary.android.utils.PreferUtils;
import com.qnadiary.android.utils.Utils;
import com.qnadiary.android.views.widgets.Button;
import com.qnadiary.android.views.widgets.EditText;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * description
 * 로그인 관련 Activity
 * @author Hanna
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText mNickNameEditText, mPasswordEditText;
    CheckBox mAutoLogin;
    Button mLoginButton, mSignUpButton;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        Intent intent = getIntent();
        boolean isLogout = intent.getBooleanExtra("check", false);
        autoLogin(isLogout);
    }

    private void initView() {
        mNickNameEditText = (EditText) findViewById(R.id.emailInput);
        mPasswordEditText = (EditText) findViewById(R.id.passwordInput);
        mAutoLogin = (CheckBox) findViewById(R.id.checkBox);
        mLoginButton = (Button) findViewById(R.id.loginButton);
        mSignUpButton = (Button) findViewById(R.id.signupButton);

        mAutoLogin.setChecked(PreferUtils.isLoginState(this));
        mLoginButton.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);
    }

    private void autoLogin(boolean isLogout) {
        if(isLogout) {
            deleteAutoLogin();
        } else if (mAutoLogin.isChecked()) {
            mNickNameEditText.setText(PreferUtils.getNickname(this));
            mPasswordEditText.setText(PreferUtils.getPassword(this));
            onLogInButtonClicked(mNickNameEditText.getString(), mPasswordEditText.getString());
        }
    }

    private void deleteAutoLogin() {
        //mAutoLogin.setChecked(false);
        PreferUtils.setLoginState(this, false);
    }

    private void onSaveData(Integer userId, String nickname, String password, boolean isAutoLogin) {
        if (userId != null) {
            PreferUtils.setUserId(this, userId);
        }
        PreferUtils.setNickname(this, nickname);
        PreferUtils.setPassword(this, password);
        PreferUtils.setLoginState(this, isAutoLogin);
    }

    // 왜 Validation에서 Preference 안에있는걸 검사하는 건가요??
    private Boolean isLoginValidation(String nickname, String password) {
        return TextUtils.isEmpty(nickname) || TextUtils.isEmpty(password);
    }

    public void onSignUpButtonClicked(final String nickname, final String password) {
        String uuid = Utils.getDevicesUUID(this);

        api(UserAPI.class).signUp(nickname, password, uuid, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if(user == null) return;
                Toast.makeText(LoginActivity.this, "회원가입이 완료되었습니다.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Response response = error.getResponse();
                String description;
                if(response.getStatus() == 409) {
                    description = response.getReason();
                } else {
                    description = "서버 연결 실패";
                }
                Toast.makeText(LoginActivity.this, description, Toast.LENGTH_SHORT).show();
                Log.d(TAG, error.toString());
            }
        });
    }

    public void onLogInButtonClicked(final String nickname, final String password) {
        // validation check를 통과하지 못한다면 함수 종료
        if(isLoginValidation(nickname, password)) return;

        api(UserAPI.class).login(nickname, password, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if(user == null) return;

                if (mAutoLogin.isChecked()) onSaveData(user.getId(), nickname, password, true);

                Toast.makeText(LoginActivity.this, "로그인 성공", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Response response = error.getResponse();
                String description;
                if(response.getStatus() == 401) {
                    description = "비밀번호가 맞지 않습니다.";
                } else if(response.getStatus() == 404) {
                    description = "아이디가 맞지 않습니다.";
                } else {
                    description = "서버 연결 실패\n네트워크 연결을 확인해주세요.";
                }
                Toast.makeText(LoginActivity.this, description, Toast.LENGTH_SHORT).show();
                Log.d(TAG, error.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        String nickname = mNickNameEditText.getString();
        String password = mPasswordEditText.getString();

        switch(v.getId()) {
            case R.id.loginButton : onLogInButtonClicked(nickname, password); break;
            case R.id.signupButton : onSignUpButtonClicked(nickname, password); break;
        }
    }
}
