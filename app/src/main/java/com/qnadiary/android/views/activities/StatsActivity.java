package com.qnadiary.android.views.activities;

import android.os.Bundle;

import com.qnadiary.android.R;
import com.qnadiary.android.base.BaseActivity;

public class StatsActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
    }
}
