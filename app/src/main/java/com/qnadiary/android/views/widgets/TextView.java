package com.qnadiary.android.views.widgets;


import android.content.Context;
import android.util.AttributeSet;

import java.text.DateFormat;
import java.util.Date;


/**
 * description
 * Android Widget을 그대로 사용할 수 없어서 상속받아서 자체 메소드 구현
 *
 * @author LeeDaYeon
 */
public class TextView extends android.widget.TextView {

    public TextView(Context c) {
        super(c);
        if (!isInEditMode()) {
            setIncludeFontPadding(false);
        }
    }

    public TextView(Context c, AttributeSet attrs) {
        super(c, attrs);
        if (!isInEditMode()) {
            setIncludeFontPadding(false);
        }
    }

    public TextView(Context c, AttributeSet attrs, int defStyleAttr) {
        super(c, attrs, defStyleAttr);
        if (!isInEditMode()) {
            setIncludeFontPadding(false);
        }
    }

    public void setText(Integer value) {
        if (value == null) return;
        setText(String.valueOf(value));
    }

    public void setText(Date value) {
        if (value == null) return;
        setText(DateFormat.getDateInstance(DateFormat.LONG).format(value));
    }

    public String getString() {
        return getText().toString();
    }

    public Integer getIntValue() {
        int value;
        try {
            value = Integer.parseInt(getText().toString());
        } catch (Exception e) {
            value = 0;
        }
        return value;
    }

    public void onUpdateIntValue() {
        onUpdateIntValue(1);
    }

    public void onUpdateIntValue(int value) {
        Integer currentValue = getIntValue();
        currentValue += value;
        setText(currentValue);
    }
}

