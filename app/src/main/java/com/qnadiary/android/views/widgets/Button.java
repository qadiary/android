package com.qnadiary.android.views.widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 * description
 * Android Widget을 그대로 사용할 수 없어서 상속받아서 자체 메소드 구현
 * @author LeeDaYeon
 */
public class Button extends android.widget.Button {

    public Button(Context c) {
        super(c);
        setIncludeFontPadding(false);
    }

    public Button(Context c, AttributeSet attrs) {
        super(c, attrs);
        setIncludeFontPadding(false);
    }

    public void setText(Integer value) {
        if (value == null) return;
        setText(String.valueOf(value));
    }

    public Integer getIntValue() {
        int value;
        try {
            value = Integer.parseInt(getText().toString());
        } catch (Exception e) {
            value = 0;
        }
        return value;
    }
}
