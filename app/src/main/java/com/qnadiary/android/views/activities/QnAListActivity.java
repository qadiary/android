package com.qnadiary.android.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.qnadiary.android.R;
import com.qnadiary.android.apis.QnaAPI;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.models.QnAData;
import com.qnadiary.android.models.User;
import com.qnadiary.android.views.adapters.QnAListAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class QnAListActivity extends BaseActivity {
    private ListView mQnAListView;
    private QnAListAdapter mQnAListAdapter;
    private List<QnAData> qnaItems = new ArrayList<>();
    private QnAData data = new QnAData();
    private User user = new User();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qna_list);
        onPullData();
        init();
    }

    private void init() {
        mQnAListView = (ListView) findViewById(R.id.qna_list_view);
        mQnAListAdapter = new QnAListAdapter(this, qnaItems);
        mQnAListView.setAdapter(mQnAListAdapter);
    }

    void onPullData() {
        api(QnaAPI.class).list_Answer(user.getId(), data.getQ_id(), new Callback<List<QnAData>>() {
            @Override
            public void success(List<QnAData> qnAData, Response response) {
                qnaItems = qnAData;
                init();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(getApplicationContext(), "실패했습니다아..",Toast.LENGTH_SHORT).show();
                Log.d("에러러ㅓㅓ", error.toString());
                Log.d("URL 정보",error.getUrl());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int groupId = 0;

        MenuItem cancelItem = menu.add(groupId, CANCEL_CODE, 0, R.string.cancel);
        cancelItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        cancelItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(CANCEL_CODE);
        finish();

        return super.onOptionsItemSelected(item);
    }
}
