package com.qnadiary.android.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.qnadiary.android.R;
import com.qnadiary.android.apis.DiaryAPI;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.common.DiaryManager;
import com.qnadiary.android.enums.Actions;
import com.qnadiary.android.enums.BundleKeys;
import com.qnadiary.android.enums.DiaryActionType;
import com.qnadiary.android.models.Diary;
import com.qnadiary.android.models.User;
import com.qnadiary.android.views.widgets.EditText;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * description
 * Diary 추가하는 Activity
 *
 * @author LeeDaYeon
 */
public class DiaryUploadActivity extends BaseActivity {

    Spinner mMonthSpinner;
    Spinner mDaySpinner;

    EditText mYearEditText;
    EditText mTitleEditText;

    EditText mContentEditText;

    private Integer year;
    private Integer month;
    private Integer day;

    DiaryActionType actionType;
    DiaryManager diaryManager;
    Diary diary;
    User user=  new User();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_upload);
        initViews();
        diaryManager = DiaryManager.getInstance(this);
        createCalendar();
    }

    private void initViews() {
        mYearEditText = (EditText) findViewById(R.id.year_edit_text);
        mTitleEditText = (EditText) findViewById(R.id.title_edit_text);
        mContentEditText = (EditText) findViewById(R.id.content_edit_text);
        mMonthSpinner = (Spinner) findViewById(R.id.month_spinner_view);
        mDaySpinner = (Spinner) findViewById(R.id.day_spinner_view);
        initSpinner(12, "%d월", mMonthSpinner);
        initSpinner(31, "%d일", mDaySpinner);
    }

    private void initSpinner(int maximum, String format, Spinner spinner) {
        ArrayList<String> tmpList = new ArrayList<>();
        for (int i = 1; i <= maximum; i++) {
            tmpList.add(String.format(format, i));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tmpList);
        spinner.setAdapter(adapter);
    }

    private void createCalendar() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);

        Intent intent = getIntent();
        int flag = intent.getIntExtra(BundleKeys.DIARY_ACTION_TYPE.getKey(), DiaryActionType.ADD.getCode());
        actionType = DiaryActionType.fromValue(flag);

        if (actionType != DiaryActionType.VIEW) {
            year = intent.getIntExtra(BundleKeys.YEAR.getKey(), year);
            month = intent.getIntExtra(BundleKeys.MONTH.getKey(), month);
            day = intent.getIntExtra(BundleKeys.DAY.getKey(), day);
        }

        String hashKey = this.diaryManager.makeKey(year, month, day);
        if (actionType == DiaryActionType.AMEND && diaryManager.containsKey(hashKey)) {
            Diary tmpDiary = diaryManager.getItem(hashKey);
            setDataOfWidget(tmpDiary);
        } else {
            setDataOfWidget(year, month, day);
        }
    }

    private void setDataOfWidget(Diary diary) {
        mYearEditText.setText(diary.getYear());
        mMonthSpinner.setSelection(diary.getMonth() - 1);
        mDaySpinner.setSelection(diary.getDay() - 1);
        mTitleEditText.setText(diary.getTitle());
        mContentEditText.setText(diary.getContents());
    }

    private void setDataOfWidget(Integer year, Integer month, Integer day) {
        mYearEditText.setText(year);
        mMonthSpinner.setSelection(month - 1);
        mDaySpinner.setSelection(day - 1);
        mTitleEditText.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int groupId = 0;
        MenuItem okItem = menu.add(groupId, SUCCESS_CODE, 0, R.string.ok);
        okItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        okItem.setIcon(android.R.drawable.ic_menu_save);

        MenuItem cancelItem = menu.add(groupId, CANCEL_CODE, 0, R.string.cancel);
        cancelItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        cancelItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Actions actions = Actions.fromValue(item.getItemId());
        switch (actions) {
            case SUCCESS:
                diary = new Diary();

                Integer monthSelectedValue = Integer.parseInt(mMonthSpinner.getSelectedItem().toString().replace("월", ""));
                Integer daySelectedValue = Integer.parseInt(mDaySpinner.getSelectedItem().toString().replace("일", ""));

                diary.setYear(mYearEditText.getIntValue());
                diary.setMonth(monthSelectedValue);
                diary.setDay(daySelectedValue);
                diary.setTitle(mTitleEditText.getString());
                diary.setContents(mContentEditText.getString());
                onAddData();
                Toast.makeText(getApplicationContext(), "저장되었습니다", Toast.LENGTH_SHORT).show();

                diaryManager.addItem(diary);

                Intent intent = new Intent();
                intent.putExtra(BundleKeys.YEAR.getKey(), diary.getYear());
                intent.putExtra(BundleKeys.MONTH.getKey(), diary.getMonth());
                setResult(SUCCESS_CODE, intent);

                finish();
                break;
            case CANCEL:
                setResult(CANCEL_CODE);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void onAddData() {
        api(DiaryAPI.class).add_Diary(user.getId(), diary.getTitle(), diary.getContents(), new Callback<Diary>() {
            @Override
            public void success(Diary diary, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(getApplicationContext(), "실패했습니다아..",Toast.LENGTH_SHORT).show();
                Log.d("에러러ㅓㅓ", error.toString());
                Log.d("URL 정보",error.getUrl());
            }
        });
    }
}
