package com.qnadiary.android.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qnadiary.android.R;
import com.qnadiary.android.models.FiveTalk;
import com.qnadiary.android.views.adapters.base.ListBaseAdapter;
import com.qnadiary.android.views.cells.FiveTalkCell;

import java.util.List;

/**
 * Created by Student on 2016-09-28.
 */
public class FiveTalkListAdapter extends ListBaseAdapter<FiveTalk> {
    public FiveTalkListAdapter(Context c) {
        super(c);
    }

    public FiveTalkListAdapter(Context c, List<FiveTalk> items) {
        super(c, items);
    }

    @Override
    protected void bindCell(int position, View view) {
        FiveTalk item = getItem(position);
        FiveTalkCell cell = (FiveTalkCell) view.getTag();
        if(item != null)
            cell.bind(item);
    }

    @Override
    protected View createCell(int position, ViewGroup viewGroup) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_five_talk, viewGroup, false);
        FiveTalkCell cell = new FiveTalkCell(getContext(), position);
        cell.init(view);
        view.setTag(cell);
        return view;
    }
}
