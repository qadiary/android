package com.qnadiary.android.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qnadiary.android.R;
import com.qnadiary.android.models.Diary;
import com.qnadiary.android.views.adapters.base.ListBaseAdapter;
import com.qnadiary.android.views.cells.DiaryCell;

import java.util.List;

/**
 * Created by Student on 2016-09-21.
 */
public class DiaryListAdapter extends ListBaseAdapter<Diary> {

    public DiaryListAdapter(Context c) {
        super(c);
    }

    public DiaryListAdapter(Context c, List<Diary> items) {
        super(c, items);
    }

    @Override
    protected void bindCell(int position, View view) {
        Diary item = getItem(position);
        DiaryCell cell = (DiaryCell) view.getTag();
        cell.bind(item);
    }

    @Override
    protected View createCell(int position, ViewGroup viewGroup) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_diary, viewGroup, false);
        DiaryCell cell = new DiaryCell(getContext(), position);
        cell.init(view);
        view.setTag(cell);
        return view;
    }
}
