package com.qnadiary.android.views.cells;

import android.content.Context;
import android.view.View;

import com.qnadiary.android.R;
import com.qnadiary.android.models.Diary;
import com.qnadiary.android.views.cells.base.BaseCell;
import com.qnadiary.android.views.widgets.TextView;

/**
 * Created by Student on 2016-09-21.
 */
public class DiaryCell extends BaseCell<Diary> {
    TextView mDateTextView;
    TextView mTitleTextView;

    public DiaryCell(Context context) {
        super(context);
    }

    public DiaryCell(Context context, int position) {
        super(context, position);
    }

    public void init(View v) {
        mDateTextView = (TextView) v.findViewById(R.id.date_text_view);
        mTitleTextView = (TextView) v.findViewById(R.id.title_text_view);
    }

    public void bind(Diary item) {
        mDateTextView.setText(item.getYear() + "년 " + item.getMonth() + "월 " + item.getDay() + "일");
        mTitleTextView.setText(item.getTitle());
        this.item = item;
    }
}
