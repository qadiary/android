package com.qnadiary.android.views.adapters.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by Student on 2016-09-04.
 */
public abstract class ListBaseAdapter<T> extends BaseAdapter {
    protected final int DEFAULT_INSERT_POSITION = 65000;
    private Context mContext;
    private List<T> mItems;

    public ListBaseAdapter(Context c) {
        this.mContext = c;
    }

    public ListBaseAdapter(Context c, List<T> items) {
        this.mContext = c;
        this.mItems = items;
    }

    public Context getContext() {
        return mContext;
    }

    public List<T> getITems() {
        return mItems;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = createCell(position, viewGroup);
        }
        bindCell(position, view);
        return view;
    }

    protected abstract void bindCell(int position, View view);

    protected abstract View createCell(int position, ViewGroup viewGroup);

    public boolean isEmpty() {
        return mItems == null || getCount() == 0;
    }

    public boolean isEmpty(List<T> items) {
        return items == null || items.size() == 0;
    }

    public boolean isEmpty(T item) {
        return item == null;
    }

    public int isPositionValidate(int position) {
        if (isEmpty()) return 0;
        return position > getCount() ? getCount() - 1 : position;
    }

    public void addAll(List<T> items) {
        addAll(DEFAULT_INSERT_POSITION, items);
    }

    public void addAll(int position, List<T> items) {
        if (position < 0 || isEmpty(items)) return;
        position = isPositionValidate(position);
        this.mItems.addAll(position, items);
        notifyDataSetChanged();
    }

    public void add(T item) {
        add(DEFAULT_INSERT_POSITION, item);
    }

    public void add(int position, T item) {
        if (isEmpty(item)) return;
        position = isPositionValidate(position);
        this.mItems.add(position, item);
        notifyDataSetChanged();
    }

    public void remove(T item) {
        this.mItems.remove(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        this.mItems.remove(position);
        notifyDataSetChanged();
    }

    public void removeAll() {
        this.mItems.clear();
        notifyDataSetChanged();
    }

    public void refresh(List<T> items) {
        removeAll();
        addAll(items);
    }

    public void change(int position, T item) {
        this.mItems.set(position, item);
        notifyDataSetChanged();
    }
}
