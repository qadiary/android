package com.qnadiary.android.views.cells.base;

import android.content.Context;
import android.widget.LinearLayout;

/**
 * Created by Student on 2016-09-04.
 */
public class BaseCell<T> extends LinearLayout {
    protected int position;
    protected T item;

    public BaseCell(Context context) {
        super(context);
    }

    public BaseCell(Context context, int position) {
        super(context);
        this.position = position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
