package com.qnadiary.android.views.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.qnadiary.android.R;
import com.qnadiary.android.base.BaseActivity;

/**
 * Created by Hanna on 2016-09-20.
 */
public class SettingActivity extends BaseActivity {
    Boolean validation;
    String[]menu = {"계정","로그아웃","주기 설정","탈퇴"};

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_setting);

        ArrayAdapter<String>Adapter;
        Adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,menu);

        ListView settingList = (ListView)findViewById(R.id.settingList);
        settingList.setAdapter(Adapter);
        settingList.setOnItemClickListener(mItemClickListner);

        settingList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        settingList.setDivider(new ColorDrawable(Color.WHITE));
        settingList.setDividerHeight(2);
    }

    AdapterView.OnItemClickListener mItemClickListner = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
        Intent intent;
        if (position==1) {
            intent = new Intent(SettingActivity.this, LoginActivity.class);
            intent.putExtra("check", true);
            startActivity(intent);
            finish();
        } else if(position==2) {
            intent = new Intent(SettingActivity.this, SetCycleActivity.class);
            startActivity(intent);
        }
        }
    };
}
