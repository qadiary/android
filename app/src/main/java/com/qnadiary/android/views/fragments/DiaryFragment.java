package com.qnadiary.android.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.qnadiary.android.Config;
import com.qnadiary.android.R;
import com.qnadiary.android.base.BaseFragment;
import com.qnadiary.android.common.DiaryManager;
import com.qnadiary.android.common.FiveTalkManager;
import com.qnadiary.android.enums.Actions;
import com.qnadiary.android.enums.BundleKeys;
import com.qnadiary.android.enums.DiaryActionType;
import com.qnadiary.android.models.FiveTalk;
import com.qnadiary.android.views.activities.DiaryUploadActivity;
import com.qnadiary.android.views.widgets.Button;
import com.qnadiary.android.views.widgets.EditText;
import com.qnadiary.android.views.widgets.TextView;

import java.util.Calendar;

/**
 * description
 *
 * @author LeeDaYeon
 */
public class DiaryFragment extends BaseFragment implements View.OnClickListener {

    private String mParam1;
    private String mParam2;

    private TextView mCalendarTextView;
    private TextView mCalendarText;
    private LinearLayout mCalendarLayout;
    private ImageButton mPrevMonthButton;
    private ImageButton mNextMonthButton;

    private ImageButton mDiaryListButton;
    private ImageButton mTimeCapsuleButton;
    private ImageButton mStatsButton;

    private LinearLayout[] theLLWeeks;
    private Button[] mDayButtons;

    private Integer year;
    private Integer month;
    private Integer day;

    private String[] days = {"일", "월", "화", "수", "목", "금", "토"};
    DiaryManager diaryManager;
    FiveTalkManager fiveTalkManager;

    public static DiaryFragment newInstance() {
        return DiaryFragment.newInstance(null, null);
    }

    public static DiaryFragment newInstance(String param1, String param2) {
        DiaryFragment fragment = new DiaryFragment();

        if (!(TextUtils.isEmpty(param1) || TextUtils.isEmpty(param2))) {
            Bundle args = new Bundle();
            args.putString(BundleKeys.YEAR.getKey(), param1);
            args.putString(BundleKeys.MONTH.getKey(), param2);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(BundleKeys.YEAR.getKey());
            mParam2 = getArguments().getString(BundleKeys.MONTH.getKey());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        diaryManager = DiaryManager.getInstance(getActivity());
        fiveTalkManager = FiveTalkManager.getInstance(getActivity());

        View resView = initViews(inflater, container);

        Calendar calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);

        mCalendarText.setText("" + day);
        mCalendarText.setTextSize(55.0f);
        mCalendarTextView.setText(String.format("%d.%d", year, month));
        mCalendarTextView.setTextSize(20.0f);

        drawCalendar(year, month);

        return resView;
    }

    private View initViews(LayoutInflater inflater, ViewGroup container) {

        View resView = inflater.inflate(R.layout.fragment_diary, container, false);

        mCalendarTextView = (TextView) resView.findViewById(R.id.calendar_text_view);
        mCalendarText = (TextView) resView.findViewById(R.id.calendar_text);
        mCalendarLayout = (LinearLayout) resView.findViewById(R.id.calendar_layout);
        mPrevMonthButton = (ImageButton) resView.findViewById(R.id.prev_month_button);
        mNextMonthButton = (ImageButton) resView.findViewById(R.id.next_month_button);

        mPrevMonthButton.setOnClickListener(this);
        mNextMonthButton.setOnClickListener(this);

        theLLWeeks = new LinearLayout[6];
        mDayButtons = new Button[6 * 7];

        initCalendar();
        return resView;
    }

    LinearLayout mHeaderLayout;

    private void initCalendar() {

        mHeaderLayout = new LinearLayout(getActivity());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPixelFromDP(50));
        mHeaderLayout.setLayoutParams(params);
        mHeaderLayout.setOrientation(LinearLayout.HORIZONTAL);


        for (int i = 0; i < 7; i++) {
            TextView tmpTV = new TextView(getActivity());
            tmpTV.setLayoutParams(new ViewGroup.LayoutParams(getScreenWidth() / 7, getPixelFromDP(50)));
            tmpTV.setText(days[i]);
            tmpTV.setTextSize(18.0f);
            tmpTV.setGravity(Gravity.CENTER);
            if (i == 0) {
                tmpTV.setTextColor(Color.RED);
            } else if (i == 6) {
                tmpTV.setTextColor(Color.BLUE);
            }
            mHeaderLayout.addView(tmpTV);
        }
        mCalendarLayout.addView(mHeaderLayout);

        for (int i = 0; i < 6; i++) {
            theLLWeeks[i] = new LinearLayout(getActivity());
            theLLWeeks[i].setLayoutParams(
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getPixelFromDP(50))
            );
            theLLWeeks[i].setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < 7; j++) {
                //int tmpColor = 100 + (i*7+j)+3;
                mDayButtons[i * 7 + j] = new Button(getActivity());
                mDayButtons[i * 7 + j].setLayoutParams(
                        new ViewGroup.LayoutParams(getScreenWidth() / 7, getPixelFromDP(50))
                );
                mDayButtons[i * 7 + j].setBackgroundColor(Color.WHITE);
                mDayButtons[i * 7 + j].setId(i * 7 + j);

                mDayButtons[i * 7 + j].setOnClickListener(this);
                if (j == 0) {
                    mDayButtons[i * 7 + j].setTextColor(Color.RED);
                } else if (j == 6) {
                    mDayButtons[i * 7 + j].setTextColor(Color.BLUE);
                }
                theLLWeeks[i].addView(mDayButtons[i * 7 + j]);
            }
            mCalendarLayout.addView(theLLWeeks[i]);
        }
    }


    public int getPixelFromDP(float aDP) {
        float scale = this.getResources().getDisplayMetrics().density;
        return (int) (aDP * scale + 0.5f);
    }

    public int getScreenWidth() {
        return this.getResources().getDisplayMetrics().widthPixels;
    }

    public void drawCalendar(Integer year, Integer month) {
        fiveTalkManager.onLoadData(Config.FIVE_TALK_DATA_URI);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);

        int tmpDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        int tmpMaxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        Integer tmpDay = 0;

        for (int i = 0; i < 6 * 7; i++) {
            if (i < tmpDayOfWeek) {
                mDayButtons[i].setText(" ");
                mDayButtons[i].setClickable(false);
            } else if (i < tmpMaxDay + tmpDayOfWeek) {
                tmpDay++;
                if (fiveTalkManager.getDiaryMap().get(fiveTalkManager.makeKey(year, month, tmpDay)) != null) {
                    mDayButtons[i].setText(String.format("%s\n⑤", tmpDay));
                } else {
                    mDayButtons[i].setText(tmpDay);
                }

                mDayButtons[i].setTextSize(13.0f);
            } else {
                mDayButtons[i].setText(" ");
                mDayButtons[i].setClickable(false);
            }
        }
    }

    private void onDayButtonClicked(Button button) {
        int tmpDay = button.getIntValue();

        if (!button.getText().equals(" ")) {
            Intent intent = new Intent(getActivity(), DiaryUploadActivity.class);
            intent.putExtra(BundleKeys.YEAR.getKey(), year);
            intent.putExtra(BundleKeys.MONTH.getKey(), month);
            intent.putExtra(BundleKeys.DAY.getKey(), tmpDay);

            String tmpKey = diaryManager.makeKey(year, month, tmpDay);

            if (diaryManager.getDiaryMap().containsKey(tmpKey)) {
                intent.putExtra(BundleKeys.DIARY_ACTION_TYPE.getKey(), DiaryActionType.AMEND.getCode());
                getActivity().startActivityForResult(intent, Actions.AMEND_DIARY.getCode());
            } else {
                intent.putExtra(BundleKeys.DIARY_ACTION_TYPE.getKey(), DiaryActionType.ADD.getCode());
                getActivity().startActivityForResult(intent, Actions.ADD_DIARY.getCode());
            }

        }
    }

    private int tmpCurrentDay;

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month - 1, 1);
            int tmpDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            int tmpMaxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            Integer tmpDay = 0;

            final Button button = (Button) v;
            for (int i = 0; i < 6 * 7; i++) {
                if (i < tmpDayOfWeek) {
                } else if (i < tmpMaxDay + tmpDayOfWeek) {
                    tmpDay++;
                    if (button.getId() == i) {
                        tmpCurrentDay = tmpDay;
                        final EditText etEdit = new EditText(getActivity());
                        etEdit.setMaxLength(5);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setTitle("5자토크");
                        dialog.setView(etEdit);
                        String tmpKey2 = fiveTalkManager.makeKey(year, month, tmpDay);
                        FiveTalk tmpDiary2 = fiveTalkManager.getDiaryMap().get(tmpKey2);
                        if (tmpDiary2 != null) {
                            etEdit.setText(tmpDiary2.getFive());
                        } else {

                        }
                        // dialog.setView(etEdit);
                        dialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (etEdit.getString().length() > 0) {
                                    FiveTalk tmpD = new FiveTalk();
                                    tmpD.setYear(year);
                                    tmpD.setMonth(month);
                                    tmpD.setDay(tmpCurrentDay);
                                    tmpD.setFive(etEdit.getString());
                                    fiveTalkManager.addItem(tmpD);

                                    drawCalendar(year, month);
                                }
                            }
                        });
                        dialog.show();
                        return;
                    }
                } else {
                }

            }
        }
        if (v instanceof ImageButton) {
            ImageButton imageButton = (ImageButton) v;
            switch (imageButton.getId()) {

                case R.id.prev_month_button:
                    month--;
                    if (month < 1) {
                        year--;
                        month = 12;
                    }
                    break;
                case R.id.next_month_button:
                    month++;
                    if (month > 12) {
                        year++;
                        month = 1;
                    }
                    break;
            }
            String text = String.format("%d.%d", year, month);
            String textD = ("" + day);
            mCalendarText.setText(textD);
            mCalendarText.setTextSize(55.0f);
            mCalendarTextView.setText(text);
            mCalendarTextView.setTextSize(20.0f);
            drawCalendar(year, month);
        }
    }
}
