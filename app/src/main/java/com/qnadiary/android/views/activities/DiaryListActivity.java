package com.qnadiary.android.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.qnadiary.android.R;
import com.qnadiary.android.apis.DiaryAPI;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.models.Diary;
import com.qnadiary.android.models.User;
import com.qnadiary.android.views.adapters.DiaryListAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DiaryListActivity extends BaseActivity {
    private ListView mDiaryListView;
    private DiaryListAdapter mDiaryListAdapter;
    private List<Diary> diaryItems = new ArrayList<>();
    private User user = new User();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_list);
        init();
    }

    private void init() {
        mDiaryListView = (ListView) findViewById(R.id.diary_list_view);
        onPullData();
        mDiaryListAdapter = new DiaryListAdapter(this, diaryItems);
        mDiaryListView.setAdapter(mDiaryListAdapter);
    }

    void onPullData() {
        api(DiaryAPI.class).list_Diary(user.getId(), new Callback<List<Diary>>() {
            @Override
            public void success(List<Diary> diary, Response response) {
                diaryItems = diary;
                init();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(DiaryListActivity.this, "실패했습니다아..", Toast.LENGTH_SHORT).show();
                Log.d("에러러ㅓㅓ", error.toString());
                //Log.d("URL 정보",error.getUrl());
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        int groupId = 0;

        MenuItem cancelItem = menu.add(groupId, CANCEL_CODE, 0, R.string.cancel);
        cancelItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        cancelItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(CANCEL_CODE);
        finish();

        return super.onOptionsItemSelected(item);
    }
}
