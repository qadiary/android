package com.qnadiary.android.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.qnadiary.android.R;
import com.qnadiary.android.apis.QnaAPI;
import com.qnadiary.android.apis.UserAPI;
import com.qnadiary.android.base.BaseFragment;
import com.qnadiary.android.models.QnAData;
import com.qnadiary.android.models.User;
import com.qnadiary.android.views.activities.MainActivity;
import com.qnadiary.android.views.activities.QnAListActivity;
import com.qnadiary.android.views.widgets.Button;
import com.qnadiary.android.views.widgets.EditText;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author LeeDaYeon
 */
public class QnAFragment extends BaseFragment implements View.OnClickListener {
    private ImageButton mSubmitButton;
    private com.qnadiary.android.views.widgets.Button mQuestionButton;
    private com.qnadiary.android.views.widgets.EditText mAnswerEditText;
    private ImageView qImageView;
    private QnAData data = new QnAData();
    private User user = new User();

    public static QnAFragment newInstance() {
        QnAFragment fragment = new QnAFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onPullQuestion();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_qna, container, false);

        mSubmitButton = (ImageButton) rootView.findViewById(R.id.submit_button);
        mQuestionButton = (Button) rootView.findViewById(R.id.question_button);
        mAnswerEditText = (EditText) rootView.findViewById(R.id.answer_edit_text);
        qImageView = (ImageView) rootView.findViewById(R.id.q_image_view);

        mSubmitButton.setOnClickListener(this);
        mQuestionButton.setOnClickListener(this);


        return rootView;
    }

    void onAddQnAData() {
        ((MainActivity)getActivity()).api(QnaAPI.class).add_Answer(44,3, "아아아아", new Callback<QnAData>() {
            @Override
            public void success(QnAData qnAData, Response response) {
                Log.d("@@@@@@@@@@@@@@@",qnAData.getQuestion());
                Log.d("##########3",qnAData.getMsg());
                Toast.makeText(getActivity(), "저장되었습니다.",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(getActivity(), "실패했습니다아..",Toast.LENGTH_SHORT).show();
                Log.d("에러러ㅓㅓ", error.toString());
                Log.d("URL 정보",error.getUrl());
            }
        });
    }

    void onPullQuestion() {
        ((MainActivity)getActivity()).api(QnaAPI.class).get_Question(user.getId(), new Callback<QnAData>() {
            @Override
            public void success(QnAData qnAData, Response response) {
                data = qnAData;
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
    String value;

    private void onSubmitButtonClicked() {
        value = mAnswerEditText.getString();
        data.setAnswer(value);
        //Toast.makeText(getActivity(), "저장되었습니다.",Toast.LENGTH_SHORT).show();
        onAddQnAData();
    }

    private void onQuestionButtonClicked() {
        Intent intent = new Intent(getActivity(), QnAListActivity.class);
        intent.putExtra("value", value);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
                onSubmitButtonClicked();
                break;
            case R.id.question_button:
                onQuestionButtonClicked();
                break;
        }
    }

}