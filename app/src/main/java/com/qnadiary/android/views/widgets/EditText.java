package com.qnadiary.android.views.widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 * description
 * Android Widget을 그대로 사용할 수 없어서 상속받아서 자체 메소드 구현
 * @author LeeDaYeon
 */
public class EditText extends android.widget.EditText {

    private int mMaxLines = 0;
    private Integer mMaxLength = null;
    private String prevString = "";

    public EditText(Context c) {
        super(c);
        setIncludeFontPadding(false);
    }

    public EditText(Context c, AttributeSet attrs) {
        super(c, attrs);
        setIncludeFontPadding(false);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (mMaxLength == null) {
            super.onTextChanged(text, start, lengthBefore, lengthAfter);
            return;
        }

        if (mMaxLength >= text.length()) {
            prevString = text.toString();
            super.onTextChanged(text, start, lengthBefore, lengthAfter);
        } else {
            setText(prevString);
            setSelection(prevString.length());
        }
    }

    public void setMaxLength(int value) {
        this.mMaxLength = value;
    }

    public void setText(Integer value) {
        if (value == null) return;
        setText(String.valueOf(value));
    }

    public String getString() {
        return getText().toString();
    }

    public Integer getIntValue() {
        int value;
        try {
            value = Integer.parseInt(getText().toString());
        } catch (Exception e) {
            value = 0;
        }
        return value;
    }
}
