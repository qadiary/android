package com.qnadiary.android.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.qnadiary.android.views.fragments.DiaryFragment;
import com.qnadiary.android.views.fragments.QnAFragment;

/**
 * 페이지 변경관련 어댑터
 *
 * @author LeeDaYeon
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a QnAFragment (defined as a static inner class below).

        if (position == 0) {
            return QnAFragment.newInstance();
        } else {
            return DiaryFragment.newInstance("A", "B");
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Q&A";
            case 1:
                return "Diary";
        }
        return null;
    }
}