package com.qnadiary.android.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.qnadiary.android.R;
import com.qnadiary.android.base.BaseActivity;
import com.qnadiary.android.enums.Actions;
import com.qnadiary.android.enums.BundleKeys;
import com.qnadiary.android.views.adapters.SectionsPagerAdapter;
import com.qnadiary.android.views.fragments.DiaryFragment;

import java.util.Calendar;
import java.util.UUID;

/***
 * 메인 액비티비티
 *
 * @author LeeDaYeon
 */
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener , ViewPager.OnPageChangeListener, View.OnClickListener  {
    public final static String TAG = "다이어리:Main";


    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private AppBarLayout mAppBarLayout;
    private Activity theActivity;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    DrawerLayout drawer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar.setTitle("QNA");

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.rgb(255, 185, 11));
        toolbar.setSubtitleTextColor(Color.rgb(255, 185, 11));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationIcon(R.drawable.icon_diary);
        toolbar.setNavigationOnClickListener(this);

        toggle = new ActionBarDrawerToggle(this, drawer, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(toggle);


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle("Q&A");
                        break;
                    case 1:
                        getSupportActionBar().setTitle("캘린더");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.setOnPageChangeListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode != SUCCESS_CODE)
//            return;
        Log.d(TAG, "onActivityResult 실행!!");
        Actions actions = Actions.fromValue(requestCode);
        switch (actions) {
            case ADD_DIARY:
                Log.d(TAG, "Add diray 실행!!");
                try {
                    Calendar calendar = Calendar.getInstance();
                    Integer year = data.getIntExtra(BundleKeys.YEAR.getKey(), calendar.get(Calendar.YEAR));
                    Integer month = data.getIntExtra(BundleKeys.MONTH.getKey(), calendar.get(Calendar.MONTH) + 1);
                } catch (Exception e) {
                    Log.d(TAG, "넘겨온 값이 없음!");
                    e.getStackTrace();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Drawable drawable = menu.findItem(R.id.action_settings).getIcon();
        //  Drawable drawable = menu.findItem(R.id.diaryList).getIcon();

        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.rgb(255, 185, 11));
        menu.findItem(R.id.action_settings).setIcon(drawable);

        return true;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        theActivity = this;

        if (id == R.id.action_settings) {
            Intent tmpl = new Intent(theActivity, DiaryUploadActivity.class);
            startActivityForResult(tmpl, Actions.ADD_DIARY.getCode());

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d(TAG, "메뉴 눌렸네~~" + item.getItemId());
        Intent intent;

        switch (item.getItemId()) {
            case R.id.diaryList:
                intent = new Intent(this, DiaryListActivity.class);
                startActivity(intent);
                break;
            case R.id.fiveList:
                intent = new Intent(this, FiveTalkListActivity.class);
                startActivity(intent);
                break;
            case R.id.timeCapsule:
                intent = new Intent(this, TimeCapsuleActivity.class);
                startActivity(intent);
                break;
            case R.id.stats:
                intent = new Intent(this, StatsActivity.class);
                startActivity(intent);
                break;
            case R.id.setting:

                intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                getSupportActionBar().setTitle("Q&A");
                break;
            case 1:
                getSupportActionBar().setTitle("캘린더");
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onClick(View v) {
        // 햄버거버튼
        //Toast.makeText(getApplicationContext(), "눌림", Toast.LENGTH_SHORT).show();
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer(Gravity.LEFT);
        } else {
            drawer.openDrawer(Gravity.LEFT);
        }
    }
}
//        mViewPager.setOnPageChangeListener(this);
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open,
//                R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
