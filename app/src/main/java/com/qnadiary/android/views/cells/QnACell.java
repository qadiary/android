package com.qnadiary.android.views.cells;

import android.content.Context;
import android.view.View;

import com.qnadiary.android.R;
import com.qnadiary.android.models.QnAData;
import com.qnadiary.android.views.cells.base.BaseCell;
import com.qnadiary.android.views.widgets.TextView;

/**
 * Created by Student on 2016-09-04.
 */
public class QnACell extends BaseCell<QnAData> {
    TextView mDateTextView;
    TextView mAnswerTextView;

    public QnACell(Context context) {
        super(context);
    }

    public QnACell(Context context, int position) {
        super(context, position);
    }

    public void init(View v) {
        mDateTextView = (TextView) v.findViewById(R.id.date_text_view);
        mAnswerTextView = (TextView) v.findViewById(R.id.answer_text_view);
    }

    public void bind(QnAData item) {
        mDateTextView.setText(item.getCreatedAt());
        mAnswerTextView.setText(item.getAnswer());
        this.item = item;
    }
}
