package com.qnadiary.android.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.qnadiary.android.DiaryApp;
import com.qnadiary.android.enums.Actions;
import com.qnadiary.android.networks.NetworkManager;

/**
 * description
 * 모든 액티비티가 상속받게되는 기본적인 부분. 액티비티의 공용적인 함수는 이곳에
 *
 * @author LeeDaYeon
 */
public class BaseActivity extends AppCompatActivity {

    protected final Integer SUCCESS_CODE = Actions.SUCCESS.getCode();
    protected final Integer CANCEL_CODE = Actions.CANCEL.getCode();
    protected final Integer FAIL_CODE = Actions.FAIL.getCode();
    protected final String TAG = this.getClass().getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected DiaryApp getApp() {
        return DiaryApp.getInstance();
    }

    protected NetworkManager getNetworkManager() {
        return NetworkManager.getInstance();
    }

    public <T> T api(Class<T> cls) {
        return getNetworkManager().getAPI(cls);
    }
}
