package com.qnadiary.android.base;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.qnadiary.android.enums.Actions;

/**
 * description
 * 모든 프레그먼트들이 상속받는 기본. 프레그먼트의 공용적인 메소드는 이곳에
 *
 * @author LeeDaYeon
 */
public class BaseFragment extends Fragment {

    protected final Integer SUCCESS_CODE = Actions.SUCCESS.getCode();
    protected final Integer CANCEL_CODE = Actions.CANCEL.getCode();
    protected final Integer FAIL_CODE = Actions.FAIL.getCode();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
