package com.qnadiary.android.common;

import android.content.Context;

import com.qnadiary.android.Config;
import com.qnadiary.android.models.QnAData;

/**
 * Created by Student on 2016-08-20.
 */
public class QnAManager  {
    public static QnAManager qnaManager = null;

    public QnAManager(Context context) {
    }

    public static QnAManager getInstance(Context context) {
        if (qnaManager == null) {
            qnaManager = new QnAManager(context);
        }

        return qnaManager;
    }
}
