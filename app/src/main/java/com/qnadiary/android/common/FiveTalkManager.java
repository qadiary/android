package com.qnadiary.android.common;

import android.content.Context;

import com.qnadiary.android.Config;
import com.qnadiary.android.models.FiveTalk;

/**
 * description
 *
 * @author JintaePang
 */
public class FiveTalkManager extends FileUtilManager<FiveTalk> {
    public static FiveTalkManager fiveTalkManager = null;

    public FiveTalkManager(Context c) {
        super(c);
        onLoadData(Config.FIVE_TALK_DATA_URI);
    }

    public static FiveTalkManager getInstance(Context context) {
        if (fiveTalkManager == null) {
            fiveTalkManager = new FiveTalkManager(context);
        }

        return fiveTalkManager;
    }

    @Override
    public String makeKey(Integer year, Integer month, Integer day) {
        return String.format(Config.FIVE_TALK_FORMAT, year, month, day);
    }

    @Override
    public int addItem(FiveTalk item) {
        String tmpKey = makeKey(item.getYear(), item.getMonth(), item.getDay());
        saveMap.put(tmpKey, item);

        onSaveData(Config.FIVE_TALK_DATA_URI);
        return 1;
    }
}
