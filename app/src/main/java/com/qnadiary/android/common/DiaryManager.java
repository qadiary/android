package com.qnadiary.android.common;

import android.content.Context;

import com.qnadiary.android.Config;
import com.qnadiary.android.models.Diary;

/**
 * description
 * 다이어리 생성, 가져오기, 저장하기를 담당
 *
 * @author LeeDaYeon
 */
public class DiaryManager extends FileUtilManager<Diary> {
    public static DiaryManager diaryManager = null;

    public DiaryManager(Context context) {
        super(context);
        onLoadData(Config.DIARY_DATA_URI);
    }

    public static DiaryManager getInstance(Context context) {
        if (diaryManager == null) {
            diaryManager = new DiaryManager(context);
        }

        return diaryManager;
    }

    @Override
    public String makeKey(Integer year, Integer month, Integer day) {
        return String.format(Config.DIARY_KEY_FORMAT, year, month, day);
    }

    @Override
    public int addItem(Diary item) {
        String tmpKey = makeKey(item.getYear(), item.getMonth(), item.getDay());
        saveMap.put(tmpKey, item);

        onSaveData(Config.DIARY_DATA_URI);
        return 1;
    }
}