package com.qnadiary.android.common;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.TreeMap;

/**
 * description
 *
 * @author JintaePang
 */
public abstract class FileUtilManager<D> {
    protected TreeMap<String, D> saveMap;
    private Context c;

    public FileUtilManager(Context c) {
        this.c = c;
        this.saveMap = new TreeMap<>();
    }

    public int onLoadData(String fileName) {
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = c.openFileInput(fileName);
        } catch (FileNotFoundException e) {
            fis = null;
        }

        if (fis == null) {
            return 0;
        }

        try {
            ois = new ObjectInputStream(fis);
            Object tmpObj = ois.readObject();
            if (tmpObj instanceof TreeMap<?, ?>) {
                saveMap = (TreeMap<String, D>) tmpObj;
            }
        } catch (IOException e) {
            return 0;
        } catch (ClassNotFoundException e) {
            return 0;
        }
        return 1;
    }

    public int onSaveData(String fileName) {
        FileOutputStream fos;
        ObjectOutputStream oos;

        try {
            Log.d("asdf", fileName);
            fos = c.openFileOutput(fileName, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            fos = null;
        }

        if (fos == null) {
            return 0;
        }

        try {
            oos = new ObjectOutputStream(fos);
            oos.writeObject(saveMap);
        } catch (IOException e) {
            return 0;
        }
        return 1;
    }

    /*public int onDeleteData(String[] deleteKey) throws IOException {

        File inputFile = new File(Config.DIARY_DATA_URI);
        File tempFile = new File("tmp"+Config.DIARY_DATA_URI);

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String currentLine;

        while((currentLine = reader.readLine()) != null) {
            // trim newline when comparing with lineToRemove
            String trimmedLine = currentLine.trim();
            if(trimmedLine.equals(deleteKey)) continue;
            writer.write(currentLine + System.getProperty("line.separator"));
        }
        writer.close();
        reader.close();
        boolean successful = tempFile.renameTo(inputFile);
    }*/

    public abstract int addItem(D item);

    public abstract String makeKey(Integer year, Integer month, Integer day);

    public D getItem(String key) {
        return saveMap.get(key);
    }

    public boolean containsKey(String key) {
        return saveMap.containsKey(key);
    }

    public TreeMap<String, D> getDiaryMap() {
        return saveMap;
    }

    public void setDiaryMap(TreeMap<String, D> diaryMap) {
        this.saveMap = diaryMap;
    }
}
