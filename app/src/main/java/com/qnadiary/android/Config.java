package com.qnadiary.android;

/**
 * description
 * 앱에서 사용되는 기본적인 상수값들을 정의해둠 ex) SERVER URI, DATA URI, HASH KEY 등등
 *
 * @author LeeDaYeon
 */
public class Config {
    public static final String DIARY_DATA_URI = "diary.dat";
    public static final String QNA_DATA_URL = "qna.dat";
    public static final String DIARY_KEY_FORMAT = "%04d/%02d/%02d";
    public static final String FIVE_TALK_DATA_URI = "five_talk.dat";
    public static final String FIVE_TALK_FORMAT = "five-talk-%04d/%02d/%02d";
    public static final String SERVER_URI = "http://ec2-52-78-163-188.ap-northeast-2.compute.amazonaws.com/api";

}
