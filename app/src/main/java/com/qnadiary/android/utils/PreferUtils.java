package com.qnadiary.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * description
 * SharedPreferences Utils
 * @author LeeDaYeon
 */
public class PreferUtils {

    public enum Keys {
        /* API 서버에 사용하는 유저 id */
        USER_ID,
        /* API 서버 자동 로그인 여부 */
        AUTO_LOGIN_STATE,
        /* User Password 정보 */
        PASSWORD,
        /* User Nickname(id) 정보 */
        NICKNAME,
    }

    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void clearPreferences(Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.clear();
        editor.apply();
    }

    /**
     * set user id
     */
    public static void setUserId(Context context, int userId) {
        putValue(context, Keys.USER_ID.name(), userId);
    }

    /**
     * get user id
     */
    public static int getUserId(Context context) {
        return getIntValue(context, Keys.USER_ID.name());
    }

    /**
     * set auto login state
     */
    public static void setLoginState(Context context, boolean loginState) {
        putValue(context, Keys.AUTO_LOGIN_STATE.name(), loginState);
    }

    /**
     * get auto login state
     */
    public static boolean isLoginState(Context context) {
        return getBooleanValue(context, Keys.AUTO_LOGIN_STATE.name());
    }

    public static void setNickname(Context context, String nickname) {
        putValue(context, Keys.NICKNAME.name(), nickname);
    }

    public static String getNickname(Context context) {
        return getValue(context, Keys.NICKNAME.name());
    }

    public static void setPassword(Context context, String password) {
        putValue(context, Keys.PASSWORD.name(), password);
    }

    public static String getPassword(Context context) {
        return getValue(context, Keys.PASSWORD.name());
    }

    /*
     * SharedPreferences set value
     */

    public static void removeValue(Context context, String key) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(key);
        editor.commit();
    }

    public static boolean putValue(Context context, String key, String value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static boolean putValue(Context context, String key, int value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static boolean putValue(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean putValue(Context context, String key, float value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static boolean putValue(Context context, String key, long value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    /*
     * SharedPreferences get value
     */

    public static final String getValue(Context context, String key) {
        return getSharedPreference(context).getString(key, "");
    }

    public static final Boolean getBooleanValue(Context context, String key, boolean defValue) {
        return getSharedPreference(context).getBoolean(key, false);
    }

    public static final Boolean getBooleanValue(Context context, String key) {
        return getBooleanValue(context, key, false);
    }

    public static final void putBooleanValue(Context context, String key, boolean bl) {
        SharedPreferences.Editor edit = getSharedPreference(context).edit();
        edit.putBoolean(key, bl);
        edit.commit();
    }

    public static final int getIntValue(Context context, String key) {
        return getSharedPreference(context).getInt(key, 0);
    }

    public static final long getLongValue(Context context, String key) {
        return getSharedPreference(context).getLong(key, 0);
    }

    public static final boolean putLongValue(Context context, String key, Long value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static final Boolean hasValue(Context context, String key) {
        return getSharedPreference(context).contains(key);
    }

}

