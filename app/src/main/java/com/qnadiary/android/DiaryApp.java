package com.qnadiary.android;

import android.app.Application;
import android.content.Context;

/**
 * description
 *
 * @author LeeDaYeon
 */
public class DiaryApp extends Application {
    private static DiaryApp mApplication;

    private static volatile Context c;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        c = getApplicationContext();
    }

    public static DiaryApp getInstance() {
        return mApplication;
    }

    public static Context getContext() {
        return c;
    }
}
